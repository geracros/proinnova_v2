# [G13]
## Instrucciones para implementar el Theme en Magento 2

#### Paso 1
#### Archivos fuera del Tema
A continuación en la tabla siguiente se describen los archivos que estan afuera del tema, y el path de donde deben de ir (copiarlos en el path indicado):

| Archivo | Path |
| ------ | ------ |
| layout1.phtml | app/code/Webkul/Marketplace/view/frontend/templates/marketplace/ |
| sellerlist.phtml | app/code/Webkul/Marketplace/view/frontend/templates/ |
| [iconos-09.svg], [iconos-10.svg], [iconos-11.svg], [man.svg], [woman.svg], [woman2.svg] | app/code/Webkul/Marketplace/view/base/web/images/landingpage1/icon/ |
| form.phtml | app/code/Agriconecta/Aboutpage/view/frontend/templates/ |
| [landingpage_one.css], [product.css] | app/code/Webkul/Marketplace/view/frontend/web/css/ |
| default.xml | app/code/Agriconecta/Aboutpage/view/frontend/layout/ |

#### Paso 2
Luego de copiar los archivos en el path indicado, copiar toda la carpeta 'proinnova_v2' en el siguiente path   [app/design/frontend/]: 

Luego de copiar la carpeta, se procede a abrir la terminal (o cmd en windows), en la raiz donde esta instalado magento, y se ejecuta el siguiente comando:

```sh
bin/magento setup:upgrade && bin/magento setup:di:compile && bin/magento setup:static-content:deploy -f && bin/magento cache:flush && bin/magento cache:clean && chmod -R 777 *;
```

#### Paso 3
Luego de los primeros pasos ejecutados, se debe de dirigir al admin de magento y seleccionar el tema que acabamos de instalar, para que se muestre en la tienda que usted seleccione.

#### Paso 4
Modificar la paina de about, copiando el contenido del archivo (about.html) que se encuentra en la raiz de este repositorio: 
Contenido ---> (Elementos) Paginas ----> (about) ---> Editar ---> (Contenido) ---> Presionar el boton (Show/Hide editor) ---> Copiar el contenido (about.html) ---> Guardar

-- Advertencia
> Contenido --> (Diseño) Configuracion --> 'Identificar la Tienda a la que se le quiere aplicar el Tema nuevo, ' ---> Editar tienda--- > (Tema por defecto)  Tema aplicado, Seleccionar el tema (AgriTienda Proinnova V2)


** Si la home page u otra pagina aun tienen el diseño del tema anterior, revisar si esas paginas tienen en (Actualizacion de diseño personalizado), seleccionado el tema anterior, esto lo encuentra en el admin de magento, en la siguiente direccion:

> Contenido ---> (Elementos) Paginas ----> (Seleccionar pagina que tiene el diseño anterior) ---> Editar ---> (Actualizacion de diseño personalizado), en 'Nuevo Tema' seleccionar 'AgriTienda Proinnova V2'